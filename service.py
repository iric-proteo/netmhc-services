"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

LGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
import random
import subprocess
from tempfile import NamedTemporaryFile

# Import third party libraries
import hug
from hug_sentry import SentryExceptionHandler
from raven import Client
from raven.transport.requests import RequestsHTTPTransport

PREDICTORS = {
    'NETMHC34': '/usr/src/app/netmhc-3.4a/netMHC -a %s -p %s',
    'NETMHC4': '/usr/src/app/netmhc-4.0a/netMHC -a %s -p %s',
    'NETMHCCONS11': '/usr/src/app/netmhccons-1.1a/netMHCcons -a %s -inptype 1 -f %s',
    'NETMHCII22': '/usr/src/app/netmhcii-2.2/netMHCII -a %s -p %s',
    'NETMHCII23': '/usr/src/app/netmhcii-2.3/netMHCII-2.3 -a %s -p %s',
    'NETMHCIIpan31': '/usr/src/app/netmhciipan-3.1a/netMHCIIpan -a %s -f %s -inptype 1',
    'NETMHCIIpan32': '/usr/src/app/netmhciipan-3.2/netMHCIIpan -a %s -f %s -inptype 1',
    'NETMHCpan28': '/usr/src/app/netmhcpan-2.8a/netMHCpan -a %s -f %s -inptype 1',
    'NETMHCpan30': '/usr/src/app/netmhcpan-3.0a/netMHCpan -a %s -f %s -inptype 1',
    'NETMHCpan40': '/usr/src/app/netmhcpan-4.0a/netMHCpan -a %s -f %s -inptype 1 -BA',

}

PREDICTORS_ALLELE = {
    'NETMHC34': 'HLA-A02:01',
    'NETMHC4': 'HLA-A0201',
    'NETMHCCONS11': 'HLA-A02:01',
    'NETMHCII22': 'HLA-DRB10101',
    'NETMHCII23': 'DRB1_0101',
    'NETMHCIIpan31': 'DRB1_0101',
    'NETMHCIIpan32': 'DRB1_0101',
    'NETMHCpan28': 'HLA-A02:01',
    'NETMHCpan30': 'HLA-A02:01',
    'NETMHCpan40': 'HLA-A02:01',
}


@hug.get(output=None, status='204 No Content')
def alive():
    """
    Simple HTTP response to check if application server is alive.
    :return: 204 No Content
    """

    return ''


def get_secret(name: str) -> str:
    """
    Gets secret from file or environment variable
    :param name: Secret name
    :return: Secret
    """

    path = os.environ.get('SECRET_PATH', '')

    file_ = os.path.join(path, name)

    if os.path.exists(file_):

        with open(file_, 'r') as f:

            return f.readline().strip()

    return os.environ[name]


@hug.get()
def hc():
    """
    Randomly checks the health of one predictor.

    :return: Empty string
    """

    predictor = random.choice(list(PREDICTORS.keys()))
    predict(predictor, PREDICTORS_ALLELE[predictor], 'PEPTIDEED')

    return ''


@hug.post(output=hug.output_format.text)
def predict(predictor: hug.types.text, allele: hug.types.text,
            peptides: hug.types.text):
    """
    Runs prediction on MHC peptides
    :param predictor: Predictor name
    :param allele: Allele name
    :param peptides: Peptides
    :return: Prediction
    """

    f = NamedTemporaryFile(delete=False, dir='/dev/shm')
    f.write(peptides.encode())
    f.close()

    args = PREDICTORS[predictor] % (allele, f.name)

    p = subprocess.Popen(args.split(' '),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT
                        )
    data, err = p.communicate()

    os.unlink(f.name)

    return data.decode()


# Sentry
if os.environ.get('SENTRY', False):
    client = Client(get_secret('SENTRY'), release=os.getenv('GIT_SHA'),
                    transport=RequestsHTTPTransport)
    handler = SentryExceptionHandler(client)
    __hug__.http.add_exception_handler(Exception, handler)
