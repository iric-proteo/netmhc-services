# NetMHC services

Combines multiples NetMHC predictors in a single service accessible via a REST API.

## Requirements

This software requires:
 
* Linux operating system (tested on Ubuntu 16.04/18.04 LTS)
* [Docker](http://docs.docker.com/engine/installation/) >= 18.6
* [Docker-compose](https://docs.docker.com/compose/install/) >= 1.24 - for test deployment on a single computer
* [Kubernetes](https://kubernetes.io/) >= 1.13 - for production deployment on a cluster
* [Helm](https://helm.sh/) - only for deployment on a Kubernetes cluster


## Getting started

First, clone this repository:

    $ git clone https://gitlab.com/iric-proteo/netmhc-services
    $ cd netmhc-services


## Download NetMHC predictors

Click the links below to retrieve the following files by navigating to the download section of each website.
Place them in the **netmhc-services** folder.

* [netMHC-3.4a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHC-3.4/)
* [netMHC-4.0a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHC-4.0/) 
* [netMHCcons-1.1a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCcons/)
* [netMHCII-2.2.Linux.tar.Z](http://www.cbs.dtu.dk/services/NetMHCII-2.2/)
* [netMHCII-2.2_data.tar.gz](http://www.cbs.dtu.dk/services/NetMHCII-2.2/)
* [netMHCII-2.3.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCII/)
* [netMHCIIpan-3.1a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCIIpan-3.1/)
* [netMHCIIpan-3.2.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCIIpan/)
* [netMHCpan-2.8a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCpan-2.8/)
* [netMHCpan-3.0a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCpan-3.0/)
* [netMHCpan-4.0a.Linux.tar.gz](http://www.cbs.dtu.dk/services/NetMHCpan/)
* [pickpocket-1.1a.Linux.tar.gz](http://www.cbs.dtu.dk/services/PickPocket/)


## Docker image build

    $ sudo docker build -t netmhc-services:latest .

## Docker-compose deployment

This deployment is suitable for testing **netmhc-services** on a single computer. It is not
configured for performance.

    $ cd deploy/docker-compose
    $ sudo docker-compose up

You can now access **netmhc-services** REST API documentation in your browser at: [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

To remove the application container

    $ sudo docker-compose down

**Note**: You can skip this deployment if you are using capaMHC 
docker-compose deployment.


## Kubernetes Helm deployment

The Helm chart provided for deployment on Kubernetes is meant as a starting 
point to deploy **netmhc-services** on your cluster. It creates a Deployment,
HorizontalPodAutoscaler and Service.

1. Upload the image built above to your image registry.
2. Changes the image name in the netmhc-deployment.yaml
3. Run the following:


    $ cd deploy/
        
    $ helm install --namespace netmhc-services --name netmhc-services helm/

To remove

    $ helm del --purge netmhc-services


## License

LGPL-3.0-or-later

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
Copyright 2015-2019 Mathieu Courcelles

CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses

Pierre Thibault's lab

IRIC - Universite de Montreal