### Predictor in this release:

* netmhc-3.4a
* netmhc-4.0a
* netmhccons-1.1a
* netmhcii-2.2
* netmhcii-2.3
* netmhciipan-3.1a
* netmhciipan-3.2
* netmhcpan-2.8a
* netmhcpan-3.0a
* netmhcpan-4.0a
