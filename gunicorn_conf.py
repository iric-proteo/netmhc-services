# Gunicorn configuration file

bind = '0.0.0.0:8000'
reload = False
timeout = 518400
max_requests = 10000

loglevel = 'info'
accesslog = '-'
access_log_format = '{"remote_address":"%(h)s","timestamp":"%(t)s","request_method":"%(m)s","url":"%(U)s","query":"%(q)s","protocol":"%(H)s","status":"%(s)s","response_length":"%(b)s","referer":"%(f)s","user_agent":"%(a)s","process_id":"%(p)s"}'
