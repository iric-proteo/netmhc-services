FROM ubuntu:18.04

# -- Install Python 2 & 3:
RUN  apt-get update && apt-get install -y \
    gawk \
    python-minimal \
    python3-minimal \
    python3-pip \
    tcsh \
    wget \
    && rm -rf /var/lib/apt/lists/*


# Prepare application directory
RUN mkdir -p /usr/src/app \
  && chmod -R 777 /usr/src/app
WORKDIR /usr/src/app

# NetMHC 3.4a
COPY netMHC-3.4a.Linux.tar.gz /usr/src/app/
RUN cat netMHC-3.4a.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHC-3.4a.Linux.tar.gz \
    && mv netMHC-3.4 netmhc-3.4a \
    && cd netmhc-3.4a/etc \
    && wget http://www.cbs.dtu.dk/services/NetMHC-3.4/net.tar.gz \
    && gunzip -c net.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm net.tar.gz
RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHC\/3.4\/netMHC-3.4/\/usr\/src\/app\/netmhc-3.4a/g' /usr/src/app/netmhc-3.4a/netMHC
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhc-3.4a/netMHC
RUN sed -i -e 's/set PYTHON = \/usr\/local\/bin\/python2.5/setenv PYTHON \/usr\/bin\/python/g' /usr/src/app/netmhc-3.4a/netMHC
RUN sed -i -e 's/set PYTHON = `which python`/setenv PYTHON \/usr\/bin\/python/g' /usr/src/app/netmhc-3.4a/netMHC


# NetMHC 4.0a
COPY netMHC-4.0a.Linux.tar.gz /usr/src/app/
RUN cat netMHC-4.0a.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHC-4.0a.Linux.tar.gz \
    && mv netMHC-4.0 netmhc-4.0a/ \
    && cd netmhc-4.0a \
    && wget http://www.cbs.dtu.dk/services/NetMHC-4.0/data.tar.gz \
    && gunzip -c data.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.tar.gz

RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHC\/4.0\/netMHC-4.0/\/usr\/src\/app\/netmhc-4.0a/g' /usr/src/app/netmhc-4.0a/netMHC
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhc-4.0a/netMHC



# pickpocket 1.1a
COPY pickpocket-1.1a.Linux.tar.gz /usr/src/app/
RUN gunzip -c pickpocket-1.1a.Linux.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm pickpocket-1.1a.Linux.tar.gz \
    && mv pickpocket-1.1 pickpocket-1.1a
RUN sed -i -e 's/\/usr\/cbs\/packages\/pickpocket\/1.1\/pickpocket-1.1/\/usr\/src\/app\/pickpocket-1.1a/g' /usr/src/app/pickpocket-1.1a/PickPocket
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/pickpocket-1.1a/PickPocket



# netMHCpan-2.8
COPY netMHCpan-2.8a.Linux.tar.gz /usr/src/app/
RUN gunzip -c netMHCpan-2.8a.Linux.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCpan-2.8a.Linux.tar.gz \
    && mv netMHCpan-2.8 netmhcpan-2.8a \
    && cd netmhcpan-2.8a \
    && wget http://www.cbs.dtu.dk/services/NetMHCpan-2.8/data.tar.gz \
    && gunzip -c data.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.tar.gz
RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHCpan\/2.8\/netMHCpan-2.8/\/usr\/src\/app\/netmhcpan-2.8a/g' /usr/src/app/netmhcpan-2.8a/netMHCpan
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhcpan-2.8a/netMHCpan



# netMHCcons-1.1a
COPY netMHCcons-1.1a.Linux.tar.gz /usr/src/app/
RUN gunzip -c netMHCcons-1.1a.Linux.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCcons-1.1a.Linux.tar.gz \
    && mv netMHCcons-1.1 netmhccons-1.1a \
    && cd netmhccons-1.1a \
    && wget http://www.cbs.dtu.dk/services/NetMHCcons-1.1/data.tar.gz \
    && gunzip -c data.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.tar.gz
RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHCcons\/1.1\/netMHCcons-1.1/\/usr\/src\/app\/netmhccons-1.1a/g' /usr/src/app/netmhccons-1.1a/netMHCcons
RUN sed -i -e 's/\/usr\/cbs\/bio\/src\/netMHCpan-2.8\/netMHCpan/\/usr\/src\/app\/netmhcpan-2.8a\/netMHCpan/g' /usr/src/app/netmhccons-1.1a/netMHCcons
RUN sed -i -e 's/\/usr\/cbs\/bio\/src\/netMHC-3.4\/netMHC/\/usr\/src\/app\/netmhc-3.4a\/netMHC/g' /usr/src/app/netmhccons-1.1a/netMHCcons
RUN sed -i -e 's/\/usr\/cbs\/bio\/src\/pickpocket-1.1\/PickPocket/\/usr\/src\/app\/pickpocket-1.1a\/PickPocket/g' /usr/src/app/netmhccons-1.1a/netMHCcons
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhccons-1.1a/netMHCcons



# NetMHCpan-3.0a
COPY netMHCpan-3.0a.Linux.tar.gz /usr/src/app/
RUN cat netMHCpan-3.0a.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCpan-3.0a.Linux.tar.gz \
    && mv netMHCpan-3.0 netmhcpan-3.0a \
    && cd netmhcpan-3.0a \
    && wget http://www.cbs.dtu.dk/services/NetMHCpan-3.0/data.tar.gz \
    && gunzip -c data.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.tar.gz
RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHCpan\/3.0\/netMHCpan-3.0/\/usr\/src\/app\/netmhcpan-3.0a/g' /usr/src/app/netmhcpan-3.0a/netMHCpan
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhcpan-3.0a/netMHCpan



# NetMHCpan-4.0a
COPY netMHCpan-4.0a.Linux.tar.gz /usr/src/app/
RUN cat netMHCpan-4.0a.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCpan-4.0a.Linux.tar.gz \
    && mv netMHCpan-4.0 netmhcpan-4.0a \
    && cd netmhcpan-4.0a \
    && wget http://www.cbs.dtu.dk/services/NetMHCpan-4.0/data.Linux.tar.gz \
    && gunzip -c data.Linux.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.Linux.tar.gz \
    && cd .. \
    && chmod 755 -R netmhcpan-4.0a

RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHCpan\/4.0\/netMHCpan-4.0/\/usr\/src\/app\/netmhcpan-4.0a/g' /usr/src/app/netmhcpan-4.0a/netMHCpan
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhcpan-4.0a/netMHCpan



# Install NetMHCII 2.2
COPY netMHCII-2.2.Linux.tar.Z /usr/src/app/
RUN cat netMHCII-2.2.Linux.tar.Z | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCII-2.2.Linux.tar.Z \
    && mv netMHCII-2.2 netmhcii-2.2

COPY netMHCII-2.2_data.tar.gz /usr/src/app/netmhcii-2.2
RUN cd netmhcii-2.2 \
    && gunzip -c netMHCII-2.2_data.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCII-2.2_data.tar.gz
RUN sed -i -e 's/\/usr\/cbs\/bio\/src\/netMHCII-2.2/\/usr\/src\/app\/netmhcii-2.2/g' /usr/src/app/netmhcii-2.2/netMHCII
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhcii-2.2/netMHCII



# Install NetMHCII 2.3
COPY netMHCII-2.3.Linux.tar.gz /usr/src/app/
RUN cat netMHCII-2.3.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCII-2.3.Linux.tar.gz \
    && mv netMHCII-2.3 netmhcii-2.3 \
    && cd netmhcii-2.3/Linux_x86_64 \
    && wget http://www.cbs.dtu.dk/services/NetMHCII-2.3/data.Linux.tar.gz \
    && gunzip -c data.Linux.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.Linux.tar.gz \
    && cd ../.. \
    && chmod 755 -R netmhcii-2.3
RUN sed -i -e 's/\/usr\/cbs\/bio\/src\/netMHCII-2.3/\/usr\/src\/app\/netmhcii-2.3/g' /usr/src/app/netmhcii-2.3/netMHCII-2.3
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhcii-2.3/netMHCII-2.3



# Install NetMHCIIpan 3.1a
COPY netMHCIIpan-3.1a.Linux.tar.gz /usr/src/app/
RUN cat netMHCIIpan-3.1a.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCIIpan-3.1a.Linux.tar.gz \
    && mv netMHCIIpan-3.1 netmhciipan-3.1a \
    && cd netmhciipan-3.1a \
    && wget http://www.cbs.dtu.dk/services/NetMHCIIpan-3.1/data.tar.gz \
    && gunzip -c data.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.tar.gz
RUN sed -i -e 's/\/usr\/cbs\/packages\/netMHCIIpan\/3.1\/netMHCIIpan-3.1/\/usr\/src\/app\/netmhciipan-3.1a/g' /usr/src/app/netmhciipan-3.1a/netMHCIIpan
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhciipan-3.1a/netMHCIIpan



# Install NetMHCIIpan 3.2
COPY netMHCIIpan-3.2.Linux.tar.gz /usr/src/app/
RUN cat netMHCIIpan-3.2.Linux.tar.gz | uncompress | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm netMHCIIpan-3.2.Linux.tar.gz \
    && mv netMHCIIpan-3.2 netmhciipan-3.2 \
    && cd netmhciipan-3.2 \
    && wget http://www.cbs.dtu.dk/services/NetMHCIIpan-3.2/data.Linux.tar.gz \
    && gunzip -c data.Linux.tar.gz | tar --no-same-owner --no-same-permissions -x -v -f - \
    && rm data.Linux.tar.gz \
    && cd .. \
    && chmod 755 -R netmhciipan-3.2
RUN sed -i -e 's/\/usr\/cbs\/bio\/src\/netMHCIIpan-3.2/\/usr\/src\/app\/netmhciipan-3.2/g' /usr/src/app/netmhciipan-3.2/netMHCIIpan
RUN sed -i -e 's/\/scratch/\/dev\/shm/g' /usr/src/app/netmhciipan-3.2/netMHCIIpan



# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV BETTER_EXCEPTIONS 1
ENV PYTHONUNBUFFERED 1
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Prepare Python environment
RUN pip3 install --no-cache-dir pipenv
COPY Pipfile* /usr/src/app/
RUN /usr/local/bin/pipenv install --system --deploy

# Deploy app
COPY gunicorn_conf.py /usr/src/app/
COPY service.py /usr/src/app/



USER www-data

CMD ["gunicorn", "-c", "./gunicorn_conf.py", "service:__hug_wsgi__"]
